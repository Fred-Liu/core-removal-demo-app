﻿using goonCA;
using goonCA.Elmo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using CoreRemovalDemo.model;

namespace CoreRemovalDemo.Viewmodel
{
    public class Machine
    {
        public Machine()
        {
            motionInitial();
            //     Home();
   
        }
        private Axis axisX;
        private Axis axisY;
        private Axis axisZ; 
        private Axis axisPin;
        public void Run(WorkParamter workPara)
        {
            
            //頂針頂起料版
            axisPin.MoveToAsync(workPara.pinUpPosition).Wait();
            //做剷除的動作
            //Z往下後，鏟起機構伸出，將鏟起機構往上拉 
            //完成整個動作
            axisZ.MoveToAsync(workPara.readyHeightPosition).Wait();
            axisY.MoveAsync(workPara.shovelForthPosition).Wait();
            axisZ.MoveToAsync(workPara.shovelPullPosition).Wait();

            //回到預備位置
            standBy(workPara);

        }

        private void Home()
        {
            //設定回HOME參數
            axisX.HomeVelParams = new VelocityParams(2000, 10000, 0.1, 0.1);
            axisY.HomeVelParams = new VelocityParams(2000, 10000, 0.1, 0.1);
            axisZ.HomeVelParams = new VelocityParams(2000, 10000, 0.1, 0.1);
            axisPin.HomeVelParams = new VelocityParams(2000, 10000, 0.1, 0.1);
            //設定回HOME模式
            //回原點
            axisZ.HomeMode = HomeModes.ORGSearchZ;
            axisY.HomeMode = HomeModes.ORGSearchZ;
            axisX.HomeMode = HomeModes.ORGSearchZ;
            axisPin.HomeMode = HomeModes.ORGSearchZ;
            //先回Z與頂針
            axisPin.HomeAsync(MotionDirections.Backward).Wait();
            axisZ.HomeAsync(MotionDirections.Backward).Wait();
            //再回XY軸
            var TX = axisX.HomeAsync(MotionDirections.Backward);
            var TY = axisY.HomeAsync(MotionDirections.Backward);
            TX.Wait();
            TY.Wait();

        }
        private void standBy(WorkParamter workPara)
        {
            //回到預備位置
            axisPin.MoveToAsync(0).Wait();
            axisZ.MoveToAsync(0).Wait();
            var TX = axisX.MoveToAsync(workPara.stanbyPosition.X);
            var TY = axisY.MoveToAsync(workPara.stanbyPosition.Y);
            TX.Wait();
            TY.Wait();

        }
        private void motionInitial()
        {
            var hostIP = IPAddress.Parse("192.168.1.100");
            var elmoIP = IPAddress.Parse("192.168.1.3");

            Maestro elmo = new Maestro(hostIP, elmoIP);
            elmo.Open();

            axisX = elmo[0];
            axisY = elmo[1];
            axisZ = elmo[2];
            axisPin = elmo[3];
            //Servo On
            axisX.Open();
            axisY.Open();
            axisZ.Open();
            axisPin.Open();
            //設定移動參數
            axisX.MotionVelParams = new VelocityParams(2000, 50000, 0.1, 0.1);
            axisY.MotionVelParams = new VelocityParams(2000, 50000, 0.1, 0.1);
            axisZ.MotionVelParams = new VelocityParams(2000, 50000, 0.1, 0.1);
            axisPin.MotionVelParams = new VelocityParams(2000, 50000, 0.1, 0.1);

        }

        
    }

    class test
    {
        private Task run(out Grabinfo info)
        {
            info = new Grabinfo();

            return Task.Run(() => { });
        }
    }
    class Grabinfo
    {
        /// <summary>
        /// 掃描矩陣的 幾之幾的位置
        /// </summary>
        public Point Index;


    }
}
