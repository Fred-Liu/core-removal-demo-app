﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CoreRemovalDemo.model;
using CoreRemovalDemo.Viewmodel;

namespace CoreRemovalDemo
{
    /// <summary>
    /// MainWindow.xaml 的互動邏輯
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        Machine machine;
        WorkParamter paramter;
        private void Btn_Initial_Click(object sender, RoutedEventArgs e)
        {
            machine = new Machine();
            paramter = new WorkParamter();

        }

        private void Btn_run_Click(object sender, RoutedEventArgs e)
        {
            machine.Run(paramter);
        }
    }
}
