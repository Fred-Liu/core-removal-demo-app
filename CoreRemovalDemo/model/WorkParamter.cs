﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace CoreRemovalDemo.model
{
    public class WorkParamter
    {
        public WorkParamter()
        {



        }
        /// <summary>
        /// XY起始位置
        /// </summary>
        public Point stanbyPosition;
        /// <summary>
        /// Z起始位置
        /// </summary>
        public double stanbyHeightPosition;
        /// <summary>
        /// 到頂針頂起高度位置
        /// </summary>
        public double pinUpPosition;
        /// <summary>
        /// 到預備剷除的高度位置
        /// </summary>
        public double readyHeightPosition;

        
        /// <summary>
        /// 往前剷除的移動距離
        /// </summary>
        public double shovelForthPosition;
        /// <summary>
        /// 到剷除拉起的上升高度位置
        /// </summary>
        public double shovelPullPosition;

    }




}
